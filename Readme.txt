Files contains basic numeric methods of calculating:
    I. Root of a function using:
       a) Bissection method
       b) Newton-Rhapson method
       c) False position method
		 d) Combined Bissection and Newton-Rhapson method
		 e) Secant method


	II. Interpolated values of functions using:
		a) Lagrange interpolation


	III. First and second derivatives value in given point using:
		a) Forward Difference Approximation
		b) Central Difference Approximation  
