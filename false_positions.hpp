#ifndef FALSE_POSITION_GUARD
#define FALSE_POSITION_GUARD

#include<cmath>

//default function
template<typename T>
T inline f(T x){
	return std::cos(x)-x;		
}

//-------------------------------------------
//with default function
template<typename T>
T root(T xl, T xr, T toll){

	while( std::abs( (xl-xr)/xr ) >= toll ){
		
		T xpom = ( xl*f(xr)-xr*f(xl) )/( f(xr)-f(xl) );	//computating next approx
		f(xpom) > 0 ? xl = xpom : xr = xpom ;		//checking which side change
	}
	return (xl+xr)/2;
}

//--------------------------------------------
//with given function
template<typename F, typename T>
T root_of(F f,T xl, T xr, T toll){

	while( std::abs( (xl-xr)/xr ) >= toll ){
		
		T xpom = ( xl*f(xr)-xr*f(xl) )/( f(xr)-f(xl) );	//computating next approx
		f(xpom) > 0 ? xl = xpom : xr = xpom ;		//checking which side change
	}
	return (xl+xr)/2;
}

#endif
