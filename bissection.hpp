#ifndef BISSECTION_GUARD
#define BISSECTION_GUARD

#include<cmath>

using std::abs;

class Both_fx_positive_or_negative{};

template<typename F, typename T>
T root_of(F f, T xl, T xr, T toll){
	T ret(xl);

	while(abs(2*(xl-xr)/(xr+xl))>=toll)
		if(f(xr)*f(xl)<0)			//check if there is a root between xl and xr
			f(xl)*f((xl+xr)/2)>0 ? xl = (xl+xr)/2 : xr = (xl+xr)/2;		//if there is root between xl and (xl+xr)/2 then eighter f(xl) or f((xl+xr)/2) is negative. So adjust xl or xr
		else throw Both_fx_positive_or_negative();

	ret = (xl+xr)/2;
	return ret;
}

#endif
