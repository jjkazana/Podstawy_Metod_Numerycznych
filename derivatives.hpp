#ifndef DERIVATIVES_GUARD
#define DERIVATIVES_GUARD

//-----------------------------------------------
//Central Difference Approximations for first and second derivative:

template<typename F, typename T>
T CDA_prim(F f, T x, T dx){
	
	return (f(x+dx) - f(x-dx)) / (2*dx);
}


template<typename F, typename T>
T CDA_bis(F f, T x, T dx){
	
	return (f(x+dx) - 2*f(x) + f(x-dx)) / (dx*dx);
}

//------------------------------------------
//Forward Difference Approximations for first and second derivative:

template<typename F, typename T>
T FDA_prim(F f, T x, T dx){

	return ( f(x+dx)-f(x) ) / dx;
}


template<typename F, typename T>
T FDA_bis(F f, T x, T dx){

	return ( f(x) - 2*f(x+dx) + f(x+2*dx) ) / (dx*dx);
}

//----------------------------------------------
//Richardson's scheme for calculating derivatives for first and second derivative:

#include<vector>
#include<cmath>
using std::vector;
using std::pow;

template<typename F, typename T>
T Richardson_prim(F f, T x, T h, int acc){

	vector<vector<T>> D( acc,vector<T>() );
	T tmp(h);

	//divide by 2 until you have the lowest (temporary) h
	for(int i=0; i<acc; i++)
		tmp /= 2;

	T tmp2 = tmp; //just for storing initial tmp value

	//calculate first set of approximations
	for(int i=0; i<acc; i++){
		D[0].push_back( CDA_prim(f,x,tmp) );
		tmp *= 2;
	}

	tmp = tmp2;
	for(int j=1; j<acc; j++){		//iterate over vectors
		for(int i=0; i<D[j-1].size()-1; i++){		//iterate over values
			D[j].push_back( (pow( (T)4.0, (T)(j) )*D[j-1][i+1] - D[j-1][i]) / 
									( pow( (T)4.0, (T)(j) ) - 1 ) 
								);		//add approximations to table D
			tmp *= 2;
		}
		tmp = tmp2;
	}
	
	return D[acc-1][0];		//last, the most accurate approximation 
}


template<typename F, typename T>
T Richardson_bis(F f, T x, T h, int acc){

	vector<vector<T>> D( acc,vector<T>() );
	T tmp(h);

	//divide by 2 until you have the lowest (temporary) h
	for(int i=0; i<acc; i++)
		tmp /= 2;

	T tmp2 = tmp; //just for storing initial tmp value

	//calculate first set of approximations
	for(int i=0; i<acc; i++){
		D[0].push_back( CDA_bis(f,x,tmp) );
		tmp *= 2;
	}

	tmp = tmp2;
	for(int j=1; j<acc; j++){		//iterate over vectors
		for(int i=0; i<D[j-1].size()-1; i++){		//iterate over values
			D[j].push_back( (pow( (T)4.0, (T)(j) )*D[j-1][i+1] - D[j-1][i]) / 
									( pow( (T)4.0, (T)(j) ) - 1 ) 
								);		//add approximations to table D
			tmp *= 2;
		}
		tmp = tmp2;
	}
	
	return D[acc-1][0];		//last, the most accurate approximation 
}

#endif














