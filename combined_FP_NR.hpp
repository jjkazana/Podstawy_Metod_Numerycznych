#ifndef COMBINED_GUARD
#define COMBINED_GUARD
#include<cmath>

template<typename F, typename T>
T f_prim(F f, T x0, T dx){
	
	return (f(x0+dx) - f(x0-dx))/(2*dx);
}

//--------------------------------
template<typename T, typename F>
T NR_step(F f, T x0, T dx){
	
	return x0-f(x0)/f_prim(f,x0,dx);
}

//----------------------------
//---------------------------
template<typename F, typename T>
T root_of(F f, T xl, T xr, T toll){
	
	T NR_approx = NR_step(f, xl, toll);
	do{
	
		NR_approx = NR_step(f, xl, toll);		//make approximation to be checked
		if(NR_approx > xl && NR_approx < xr)	//check approximation
			 f(xl)*f(NR_approx)>0 ? xl = NR_approx : xr = NR_approx;		//adjust borders 
			else{						//approximation wasn't good, so:				
				f(xl)*f((xl+xr)/2)>0 ? xl = (xl+xr)/2 : xr = (xl+xr)/2;//conduct bissection
				NR_approx = xr;	//set NR_approx to neutral value for condition
				}	
			
	}while( std::abs( (xl-NR_approx)/xl ) > toll );

	return xl;
}

#endif
