#ifndef NEWTON_RHAPSON_GUARD
#define NEWTON_RHAPSON_GUARD

#include<cmath>

//-----------------------------
template<typename F, typename T>
T f_prim(F f, T x0, T dx){
	
	return (f(x0+dx) - f(x0-dx))/(2*dx);
}

//--------------------------------
template<typename T, typename F>
T NR_step(F f, T x0, T dx){
	
	return x0-f(x0)/f_prim(f,x0,dx);
}

//------------------------------
template<typename F, typename T>
T root_of(F f, T x0, T toll){	
	T ret = NR_step(f, x0, toll);

	while( std::abs( (x0-ret)/ret ) >= toll){
		x0 = ret;
		ret = NR_step(f,x0,toll);
	}

	return ret;
}
#endif
