#ifndef SECANT_GUARD
#define SECANT_GUARD

#include<cmath>

template<typename F, typename T>
T root_of(F f, T x0, Tx1, T toll){

	while( std::abs( (x0-x1)/x1 ) >= toll ){
		
		T x = ( x0*f(x1)-x1*f(x0) )/( f(x1)-f(x0) );
		x0 = x1;
		x1 = x;
	}

	return x1;
}

#endif
