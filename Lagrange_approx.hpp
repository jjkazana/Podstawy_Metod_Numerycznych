/*
	Works for std::vector<T> as cointainer, not tested for others containers.

*/

#ifndef LAGRANGE_GUARD
#define LAGRANGE_GUARD

//accept any numeric type to count Lagrange polynomial I(argument_for_L_polynomial, xj_to_not_divide_by_0, vector_of_discrete_function_values)
template<typename T, typename C>
T I( T x, T xj, C& v ){

	T ret(1);
	for(auto xi:v)
		if( xi != xj ) ret *= (x-xi)/(xj-xi);

	return ret;
}

//return approx value of function f given by pairs (xi[i],fi[i]), (using Lagrange approximation) in point x
template<typename T, typename C>
T Lagrange(T x, C& xi, C& fi){

	T ret(0);
	for(int i=0; i<xi.size() && i<fi.size(); i++)
		ret += I(x, xi[i], xi)*fi[i];
		
	return ret;
}


//accept any numeric value, return vector of interpolated points of function f, given by points ( xi[i],fi[i] ), on the section <x0,xe>
template<typename T, typename C>
vector<T> L_v(C& xi, C& fi, T x0, T xe, T step){
	
	vector<T>ret;

	for(; x0<=xe; x0+=step)
		ret.push_back(Lagrange(x0, xi, fi));			
	
	return ret;
}


#endif
